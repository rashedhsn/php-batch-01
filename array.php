<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 8/13/16
 * Time: 8:58 PM
 */

//index array
$number = array('one', 'two', 'three');
//echo count($number);

/*$number[2];
print_r($number);*/

//var_dump($number);


//echo $number[0];

//associative array
/*$age = array(
    "Peter"=>"35",
    "Ben"=>"37",
    "Joe"=>"43"
);
print_r($age);
echo $age['Peter'];*/


//multidimentional array

$age = array(
    "Peter"=>array(
        "Ben"=>"66",
        "Joe"=>"88",
        'Rashed'=> array(
            "Ben"=>"22",
            "Joe"=>"44",
        ),
    ),
    "Ben"=>"37",
    "Joe"=>"43"
);
echo $age['Peter']['Joe'];
//echo $age['Peter']['Rashed']['Joe'];