<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 8/13/16
 * Time: 8:03 PM
 */

$favcolor = "red";

switch ($favcolor) {
    case "red":
        echo "Your favorite color is red!";
        break;
    case "blue":
        echo "Your favorite color is blue!";
        break;
    case "green":
        echo "Your favorite color is green!";
        break;
    default:
        echo "Your favorite color is neither red, blue, nor green!";
}
?>