<?php
/**
 * Created by PhpStorm.
 * User: muhammad
 * Date: 8/20/16
 * Time: 8:02 PM
 */
?>
<style type="text/css">
    fieldset {
        width: 500px;
    }
    fieldset>div>label {
        width: 150px; display: block; float: left;
    }
    fieldset>div.gender>label{width: auto;
        float: none;}

</style>

<form id="registrationForm" name="registrationForm" enctype="multipart/form-data" method="get" action="form/confirmation.php">
    <h3>Registration Form</h3>
    <fieldset>
        <div>
            <label>Name: </label>
            <input name="name" type="text" placeholder="Enter your name">
        </div><br />

        <div>
            <label>Father Name: </label>
            <input name="fatherName" type="text" value="Father name">
        </div><br />

        <div>
            <label>Age: </label>
            <input name="age" type="text" value="20" readonly>
        </div><br />

        <div>
            <label>Gender: </label>
            <div class="gender">
                <label>Male</label>
                <input name="gender" type="radio" value="Male">
                <label>Female</label>
                <input name="gender" type="radio" value="Female">
            </div>

        </div><br />

        <div>
            <label>Bio</label>
            <textarea name="bio" cols="20" rows="5"></textarea>
        </div><br />

        <div>
            <label>District</label>
            <select name="district">
                <option>Select District</option>
                <option value="dhaka">Dhaka</option>
                <option selected value="barishal">Barishal</option>
                <option value="comilla">Comilla</option>
            </select>
        </div><br />
        <div>
            <label>Photo: </label>
            <input type="file" name="photo" />
        </div><br />
        <input type="submit" name="submit" value="Save">

    </fieldset>

</form>